<?php





// Start Session
session_start();

// Database connection
require __DIR__ . '/config/db_connection.php';
$db = DB();

// Application library ( with DemoLib class )
require __DIR__ . '/library/library.php';
$app = new DemoLib($db);

$login_error_message = '';

// check Login request
if (!empty($_POST['btnLogin'])) {

    $username = trim($_POST['username']);
    $password = trim($_POST['password']);

    if ($username == "") {
        $login_error_message = 'Username field is required!';
    } else if ($password == "") {
        $login_error_message = 'Password field is required!';
    } else {
        $user_id = $app->Login($username, $password); // check user login
        if($user_id > 0)
        {
            $_SESSION['user_id'] = $user_id; // Set Session
            header("Location: validate_login.php"); // Redirect user to validate auth code
        }
        else
        {
            $login_error_message = 'Invalid login details!';
        }
    }
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Belépés</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row jumbotron">
        <div class="col-md-12">
            <h2>
                Belépőrendszer kétlépcsős Google hitelesítéssel 
            </h2>
        
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-md-offset-3 well">
            <h4>Belépés</h4>
            <?php
            if ($login_error_message != "") {
                echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $login_error_message . '</div>';
            }
            ?>
            <form action="index.php" method="post">
                <div class="form-group">
                    <label for="">Felhasználónév/Email</label>
                    <input type="text" name="username" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="">Jelszó</label>
                    <input type="password" name="password" class="form-control"/>
                </div>
                <div class="form-group">
                    <input type="submit" name="btnLogin" class="btn btn-primary" value="Belépés"/>
                </div>
            </form>
            <div class="form-group">
                Még nem regisztrált? <a href="registration.php">Regisztráció</a>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
     
      
      <div class="panel-heading">Felhasználók:</div>
      <!-- Table -->
      <?php
  
         echo "<table class=table style='border: solid 1px black;'>";
         echo "<tr><th>Id</th><th>Név</th><th>Felhasználónév</th></tr>";
        
        class TableRows extends RecursiveIteratorIterator { 
            function __construct($it) { 
                parent::__construct($it, self::LEAVES_ONLY); 
            }
        
            function current() {
                return "<td style='width: 150px; border: 1px solid black;'>" . parent::current(). "</td>";
            }
        
            function beginChildren() { 
                echo "<tr>"; 
            } 
        
            function endChildren() { 
                echo "</tr>" . "\n";
            } 
        } 
        
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "google_auth";
        
     
        
        try {
            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password, 
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") 
           
            );
            
     
           
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $conn->prepare("SELECT id, name, username FROM users"); 
            $stmt->execute();
        
            // set the resulting array to associative
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC); 
        
            foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) { 
                echo $v;
            }
        }
        catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $conn = null;
        echo "</table>";
       
           
      ?>
      </div>
    </div>

    <hr>
    
</div>

</body>
</html>