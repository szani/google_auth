<?php

// Start Session
session_start();

// check user login
if(empty($_SESSION['user_id']))
{
    header("Location: index.php");
}

// Database connection
require __DIR__ . '/config/db_connection.php';
$db = DB();

// Application library ( with DemoLib class )
require __DIR__ . '/library/library.php';
$app = new DemoLib($db);
$user = $app->UserDetails($_SESSION['user_id']);

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>User Profile</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="row jumbotron">
        <div class="col-md-12">
            <h2>
                
            </h2>
            <p>
                
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 col-md-offset-3 well">
            <h2>Felhasználói profil</h2>
            <h4>Üdvözlöm <?php echo $user->name; ?>!</h4>
            <p>Az Ön adatai:</p>
            <p>Név: <?php echo $user->name; ?></p>
            <p>Felhasználónév: <?php echo $user->username; ?></p>
            <p>Email: <?php echo $user->email; ?></p>
            <br>
            Kattintson ide a <a href="logout.php">Kilépéshez</a>
        </div>
    </div>

    <hr>
    
</div>

</body>
</html>